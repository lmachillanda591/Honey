require 'chingu'
include Gosu

class Game < Chingu::Window
  def initialize
    super(1000, 255)
    switch_game_state(Background)
  end
end

class Background < Chingu::GameState
  def initialize
    super #Parallax es una clase de chingu para colocar varias capas(animaciones/background etc)
    @parallax = Chingu::Parallax.create(:x => 0, :y => 0, :rotacion_center => :top_left)
    @parallax << {:image => "Img/background1.jpg"} #1era forma
    #@parallax.add_layer(:image => "Img/background1.jpg") #2da forma
    self.input = [ :holding_left, :holding_right, :holding_up, :holding_down ]
  end

  def holding_left; @parallax.camera_x -=2; end;
  def holding_right; @parallax.camera_x +=2; end;
  def holding_up; @parallax.camera_y -=2; end;
  def holding_down; @parallax.camera_y +=2; end;
end

Game.new.show