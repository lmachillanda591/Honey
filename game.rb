require 'chingu' 
include Gosu

class Game < Chingu::Window ####---1
  def initialize
    super(600, 600, true) #para seguir escribiendo en el constructor sin eliminar algo establecido en la liberería
    self.caption = "Hola este es mi primer juego en ruby"
    self.input = {:escape => :exit} #viene en chingu
    push_game_state(Inicio)
  end
end

class Player < Chingu::GameObject ####---4
  traits :collision_detection, :bounding_circle
  def initialize(options = {})
    super
    @image = Image["Img/rigovather.png"]
  end
  
  def holding_left; @x -= 3; end #una manera de hacer un metodo
  def holding_right; @x += 3; end 
  def holding_up; @y -= 3; end 
  def holding_down; @y += 3; end 

  def space
    Star.create(:x => @x, :y => @y)
    #Star.create(:x => @x +20, :y => @y)
    #Star.create(:x => @x -20, :y => @y)
  end

  def update
     if @x < 0 || @x>$window.width; @x %= $window.width; end 
     if @y < 0 || @y>$window.width; @y %= $window.height; end 
  end
end

class Eye < Chingu::GameObject ####---8
  traits :velocity, :collision_detection, :bounding_circle #2,3 detectar si nuestro objetos estan chocando 
  def initialize(options = {})
    super
    @image = Image["Img/eye_tab.png"] 
    self.velocity_x = rand(-1..1)
    self.velocity_y = rand(-1..1)
  end

  def update
    if @x < 0 || @x>$window.width; @x %= $window.width; end #modulo aritmetico (un operador que devuelve el resto que falta con cierto numero)  
     if @y < 0 || @y>$window.width; @y %= $window.height; end 
  end
end

class Star < Chingu::GameObject ####---5
  traits :collision_detection, :bounding_circle #1,2 detectar si nuestro objetos estan chocando 
  def initialize(options = {})
    super
    @image = Image["Img/star.png"]
  end

  def update
    @x += 10
    if outside_window?
      self.destroy #borrar los objetos cuando salgan de pantalla para liberar memoria RAM
    end 
  end
end

class Inicio < Chingu::GameState ####---2 
  #Pantalla de incio
  def initialize
    super
    Chingu::Text.create(:text => "Aprieta F1 para continuar", :x => 180, :y => $window.height/2, :size=>30)
    self.input = {:f1 => Play} #llama a la clase play al apretar f1
  end
end

class Play < Chingu::GameState ####---3
  def initialize
    super
    Chingu::Text.create(:text => "Aprieta P para pausa y R para resetear") #crear un texto de referencia
    @player = Player.create(:x => 200, :y => 200) #posicion de el objeto en la ventana 
    @player.input = [ :holding_left, :holding_right, :holding_up, :holding_down, :space ]
    self.input = {:p => Pause, :r => :reset_game } #llama a la clase pause y al metodo reset_game
    4.times { Eye.create(:x => rand($window.width), :y => rand($window.height) ) } #8---(4 enemigos)
  end

  def reset_game ####---7
    Eye.destroy_all
    Star.destroy_all
    @player.x = $window.width/2 #reposiciona al objeto Player
    @player.y = $window.height * 0.95 #''
    4.times { Eye.create(:x => rand($window.width), :y => rand($window.height) ) } #8---(4 enemigos)
  end

  def update
    super
    Star.each_collision(Eye) do |star, eye| star.destroy; eye.destroy; end  #Cada vez que las estrellas colisionen con los ojos
    Player.each_collision(Eye) do |player, eye| push_game_state(Lose) end
  end
end

class Lose < Chingu::GameState
  def initialize 
    super
    Chingu::Text.create(:text => "Apriete C para continuar", :x => 180, :y => $window.height/2, :size=>30)
    self.input = {:c => lambda{pop_game_state}} #lambda es una manera de llamar algunos metodos
  end
end


class Pause < Chingu::GameState ####---6
  def initialize
    super
    self.input = {:p => :sinpause} #Quitar el pause
  end

  def sinpause
    pop_game_state #elimina el estado actual del juego
  end

  def draw
    super
    previous_game_state.draw #congelar objetos
  end
end

Game.   new.show