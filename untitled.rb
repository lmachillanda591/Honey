require 'chingu'
include Gosu

class Game < Chingu::Window
  def initialize
    super #para seguir escribiendo en el constructor sin eliminar algo establecido en la liberería
    self.caption = "Hola este es mi primer juego en ruby"

    @player = Player.create(:x => 200, :y => 200) #posicion de el objeto en la ventana
    @player.input = [ :holding_left, :holding_right, :holding_up, :holding_down, :space ]
  end
end

class Player < Chingu::GameObject
  def initialize(options = {})
    super
    @image = Image["Img/rigovather.png"]
  end

=begin
  def move_left
    @x -= 3
  end
=end #Una manera de hacerla con hash en player.input like that: @player.input = { :holding_left => :move_left}

  def holding_left; @x -= 3; end #una manera de hacer un metodo
  def holding_right; @x += 3; end 
  def holding_up; @y -= 3; end 
  def holding_down; @y += 3; end 
  def space
    Star.create(:x => @x, :y => @y)
    Star.create(:x => @x +20, :y => @y)
    Star.create(:x => @x -20, :y => @y)
  end
end

class Star < Chingu::GameObject
  def initialize(options = {})
    super
    @image = Image["Img/eye_tab.png"]
  end

  def update
    @y -= 10
    if outside_window?
      self.destroy
    end
  end
end

Game.new.show